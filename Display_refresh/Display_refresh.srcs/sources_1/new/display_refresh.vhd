----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Design Name: 
-- Module Name: 
-- Project Name: 
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_unsigned.all;
use IEEE.numeric_std.all;

entity display_refresh is
    Generic( nsegments : positive := 7);
    
    Port ( 
           clk                : in STD_LOGIC;
           segment_uds       : in STD_LOGIC_VECTOR (nsegments-1 downto 0);
           segment_dec        : in STD_LOGIC_VECTOR (nsegments-1 downto 0);
           display_number     : out STD_LOGIC_VECTOR (nsegments-1 downto 0);
           display_selection  : out STD_LOGIC_VECTOR (3 downto 0));
end display_refresh;

architecture behavioral of display_refresh is

signal cnt_i:std_logic_vector(1 downto 0):="00";
begin
 process(clk)
 begin
  if rising_edge(clk) then
   cnt_i<=cnt_i+1;
  end if;
  end process;
  with cnt_i select
   display_number<=segment_uds when "00",
                   segment_dec when "01",
                   (others=>'1') when others;
  with cnt_i select
   display_selection<="0001" when "00",
                      "0010" when "01",
                      "0100" when "10",
                      "1000" when others;
  end behavioral;
   